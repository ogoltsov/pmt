CREATE TABLE Priority_level (
  id    BIGINT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  title VARCHAR(250) NOT NULL,
  level INTEGER      NOT NULL,
  UNIQUE (title, level),
  CHECK (level >= 0)
);