INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Kirill', 'Ogoltsov', (SELECT R.id
                         FROM ROLE R
                         WHERE R.access_level = 0)
);
INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Aizada', 'Zeken', (SELECT R.id
                      FROM ROLE R
                      WHERE R.access_level = 1)
);
INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Evgeniy', 'Frolov', (SELECT R.id
                        FROM ROLE R
                        WHERE R.access_level = 1)
);
INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Ildar', 'Salihov', (SELECT R.id
                       FROM ROLE R
                       WHERE R.access_level = 2)
);
INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Zarina', 'Zhienbaeva', (SELECT R.id
                           FROM ROLE R
                           WHERE R.access_level = 2)
);
INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Ilya', 'Bondarenko', (SELECT R.id
                         FROM ROLE R
                         WHERE R.access_level = 8)
);
INSERT INTO Users (firstname, lastname, ROLE) VALUES (
  'Xeniya', 'Petrenko', (SELECT R.id
                         FROM ROLE R
                         WHERE R.access_level = 9)
);