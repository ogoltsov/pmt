CREATE TABLE Task (
  id           BIGINT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  title        VARCHAR(1000) NOT NULL,
  description  TEXT          NOT NULL,
  created_date TIMESTAMP     NOT NULL,
  start_date   TIMESTAMP     NOT NULL,
  ending_date  TIMESTAMP     NOT NULL,
  status_id    INTEGER,
  priority_id  INTEGER,
  FOREIGN KEY (status_id) REFERENCES Status (id),
  FOREIGN KEY (priority_id) REFERENCES Priority_level (id),
  UNIQUE (title),
  CHECK (created_date <= Task.start_date),
  CHECK (ending_date > Task.start_date)
);