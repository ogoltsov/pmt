CREATE TABLE Task_type (
  id          INT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  title       VARCHAR(150) NOT NULL,
  description TEXT         NOT NULL,
  UNIQUE (title),
  CHECK (length(description) >= 5),
  CHECK (length(title) >= 1)
);