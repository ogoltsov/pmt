CREATE TABLE Projects (
  id                  INT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  title               VARCHAR(500) NOT NULL,
  description         TEXT,
  start_project_date  DATE  NOT NULL,
  ending_project_date DATE  NOT NULL,
  UNIQUE (title)
);
--   TODO: Add constrains that ending_project_date must be more than start_project_date