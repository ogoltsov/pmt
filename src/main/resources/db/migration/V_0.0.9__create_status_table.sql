CREATE TABLE Status (
  id          INT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  title       VARCHAR(250) NOT NULL,
  description TEXT         NOT NULL,
  UNIQUE (title),
  CHECK (length(description) > 5)
);