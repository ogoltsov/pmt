CREATE TABLE Project_comments (
  project_id INTEGER,
  comment_id INTEGER,
  FOREIGN KEY (project_id) REFERENCES Projects (id),
  FOREIGN KEY (comment_id) REFERENCES Comments (id) ON DELETE CASCADE
);
