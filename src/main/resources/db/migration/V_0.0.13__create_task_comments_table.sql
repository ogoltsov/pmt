CREATE TABLE Task_comments (
  task_id    INTEGER,
  comment_id INTEGER,
  FOREIGN KEY (task_id) REFERENCES Task (id),
  FOREIGN KEY (comment_id) REFERENCES Comments (id)
);
