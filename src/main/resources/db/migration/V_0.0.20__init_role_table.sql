INSERT INTO Role (access_level, title) VALUES (0, 'God');
INSERT INTO Role (access_level, title) VALUES (1, 'Admin');
INSERT INTO Role (access_level, title) VALUES (2, 'Moderator');
INSERT INTO Role (access_level, title) VALUES (8, 'User');
INSERT INTO Role (access_level, title) VALUES (9, 'Guest');

