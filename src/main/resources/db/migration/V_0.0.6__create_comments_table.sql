CREATE TABLE Comments (
  id      BIGINT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  message TEXT NOT NULL,
  user_id INTEGER,
  date    TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES Users (id)
);