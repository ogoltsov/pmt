CREATE TABLE role (
  ID           INT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  ACCESS_LEVEL INTEGER,
  TITLE        VARCHAR(100),
  CHECK (access_level >= 0),
  UNIQUE (access_level)
);