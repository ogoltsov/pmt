CREATE TABLE Users (
  id        INT PRIMARY KEY DEFAULT nextval('ids_sequence'),
  firstname VARCHAR(150) NOT NULL,
  lastname  VARCHAR(150) NOT NULL,
  role      INT REFERENCES role (id),
  image     BYTEA           DEFAULT NULL
);