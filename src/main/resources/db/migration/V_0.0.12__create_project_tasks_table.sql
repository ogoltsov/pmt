CREATE TABLE Project_tasks (
  project_id INTEGER,
  task_id    INTEGER,
  FOREIGN KEY (project_id) REFERENCES Projects (id),
  FOREIGN KEY (task_id) REFERENCES Task (id)
);