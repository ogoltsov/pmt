package kz.kstu.domain;

import javax.persistence.*;

@Entity
@Table(name = "PRIORITY_LEVEL")
@Access(AccessType.PROPERTY)
public class Priority extends BaseEntity {

    private String title;
    private Integer level;

    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "LEVEL")
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
