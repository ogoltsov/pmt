package kz.kstu.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "users")
@Access(AccessType.PROPERTY) //TODO: <-- FIX THIS SHIT!!
public class User extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -8035946990254429753L;
    private String firstname;
    private String lastname;
    private Role role;
    private Set<Comment> comments;

    public User() {
    }

    @Column(name = "FIRSTNAME")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Column(name = "LASTNAME")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ROLE")
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @Transient
    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }
}
