package kz.kstu.domain;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
//@Access(AccessType.PROPERTY)
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 4765690722716965574L;
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "id_seq", allocationSize = 1, sequenceName = "ids_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    private Integer id;

    public BaseEntity() {
    }

    public BaseEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
