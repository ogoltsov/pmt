package kz.kstu.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "COMMENTS")
@Access(AccessType.PROPERTY)
public class Comment extends BaseEntity {

    private String message;
    private User user;
    private Date date;

    @Column(name = "MESSAGE")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "DATE")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
