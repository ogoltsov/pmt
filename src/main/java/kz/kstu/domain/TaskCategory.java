package kz.kstu.domain;

import javax.persistence.*;

@Entity
@Table(name = "TASK_TYPE")
@Access(AccessType.PROPERTY)
public class TaskCategory extends BaseEntity {

    private String title;
    private String description;

    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
