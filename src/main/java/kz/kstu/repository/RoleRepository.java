package kz.kstu.repository;

import kz.kstu.domain.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RoleRepository extends CrudRepository<Role, Integer> {

    List<Role> findAll();

    Role findOne(Integer id);
}
