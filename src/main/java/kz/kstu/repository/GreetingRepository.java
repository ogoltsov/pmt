package kz.kstu.repository;

import kz.kstu.domain.Greeting;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GreetingRepository extends CrudRepository<Greeting, Integer> {

    List<Greeting> findAll();
}
