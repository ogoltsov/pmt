package kz.kstu.repository;

import kz.kstu.domain.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskRepository extends CrudRepository<Task, Integer> {

    List<Task> findAll();

    Task findOne(Integer id);

}
