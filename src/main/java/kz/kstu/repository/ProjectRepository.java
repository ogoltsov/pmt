package kz.kstu.repository;

import kz.kstu.domain.Project;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProjectRepository extends CrudRepository<Project, Integer> {

    List<Project> findAll();

    Project findOne(Integer id);

}
