package kz.kstu.repository;

import kz.kstu.domain.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Integer> {

    List<Comment> findAll();

    Comment findOne(Integer id);

}
