package kz.kstu.repository;

import kz.kstu.domain.TaskCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TaskCategoryRepository extends CrudRepository<TaskCategory, Integer> {

    List<TaskCategory> findAll();

    TaskCategory findOne(Integer id);

}
