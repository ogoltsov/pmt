package kz.kstu.repository;

import kz.kstu.domain.Priority;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PriorityLevelRepository extends CrudRepository<Priority, Integer> {

    List<Priority> findAll();

    Priority findOne(Integer id);

}
