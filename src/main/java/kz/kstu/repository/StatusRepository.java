package kz.kstu.repository;

import kz.kstu.domain.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StatusRepository extends CrudRepository<Status, Integer> {

    List<Status> findAll();

    Status findOne(Integer id);

}
