package kz.kstu;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@SpringBootApplication
@PropertySource(value = {"classpath:application.properties"})
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }

    @Bean
    @Profile("production")
    public Flyway flyway(DataSource theDataSource) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(theDataSource);
        flyway.setLocations("classpath:db/migration");
        flyway.setSqlMigrationPrefix("V_");
        flyway.setSqlMigrationSeparator("__");
        flyway.setSqlMigrationSuffix(".sql");
        flyway.clean();
        flyway.migrate();

        return flyway;
    }

}
