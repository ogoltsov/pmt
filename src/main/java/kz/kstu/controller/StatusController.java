package kz.kstu.controller;

import kz.kstu.domain.Status;
import kz.kstu.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/status")
public class StatusController {

    @Autowired
    private StatusRepository repository;

    @RequestMapping("")
    public List<Status> findAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public Status findOne(@PathVariable Integer id) {
        return repository.findOne(id);
    }

}
