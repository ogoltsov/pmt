package kz.kstu.controller;

import kz.kstu.domain.Priority;
import kz.kstu.repository.PriorityLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/priorities")
public class PriorityLevelController {

    @Autowired
    private PriorityLevelRepository repository;

    @RequestMapping("")
    public List<Priority> findAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public Priority findOne(@PathVariable Integer id) {
        return repository.findOne(id);
    }

}
