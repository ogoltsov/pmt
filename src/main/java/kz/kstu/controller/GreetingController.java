package kz.kstu.controller;

import kz.kstu.domain.BaseEntity;
import kz.kstu.domain.Greeting;
import kz.kstu.repository.GreetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/greeting")
public class GreetingController extends BaseEntity {

    @Autowired
    private GreetingRepository repository;

    @RequestMapping("")
    public List<Greeting> greet(@RequestParam String name) {
        return repository.findAll();
    }

}
