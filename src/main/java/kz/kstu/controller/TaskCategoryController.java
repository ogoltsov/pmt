package kz.kstu.controller;

import kz.kstu.domain.TaskCategory;
import kz.kstu.repository.TaskCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/taskcategory")
public class TaskCategoryController {

    @Autowired
    private TaskCategoryRepository repository;

    @RequestMapping("")
    public List<TaskCategory> getAll(){
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public TaskCategory getOne(@PathVariable Integer id){
        return repository.findOne(id);
    }

}
