package kz.kstu.controller;

import kz.kstu.domain.Comment;
import kz.kstu.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentRepository repository;

    @RequestMapping("")
    public List<Comment> getAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public Comment getOne(@PathVariable Integer id) {
        return repository.findOne(id);
    }
}
