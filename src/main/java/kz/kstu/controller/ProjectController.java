package kz.kstu.controller;

import kz.kstu.domain.Project;
import kz.kstu.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectRepository repository;

    @RequestMapping("")
    public List<Project> getAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public Project findById(@PathVariable Integer id) {
        Project project = repository.findOne(id);
        return project;
    }


}
