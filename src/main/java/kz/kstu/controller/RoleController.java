package kz.kstu.controller;

import kz.kstu.domain.Role;
import kz.kstu.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleRepository repository;

    @RequestMapping("")
    public List<Role> getAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public Role getAll(@PathVariable Integer id) {
        return repository.findOne(id);
    }

}
