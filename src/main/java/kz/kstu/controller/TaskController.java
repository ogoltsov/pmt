package kz.kstu.controller;

import kz.kstu.domain.Task;
import kz.kstu.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskRepository repository;

    @RequestMapping("")
    public List<Task> findAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public Task findOne(@PathVariable Integer id) {
        return repository.findOne(id);
    }

}