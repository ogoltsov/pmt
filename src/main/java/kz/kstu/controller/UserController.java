package kz.kstu.controller;

import kz.kstu.domain.User;
import kz.kstu.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/user")
@RestController
public class UserController {

    private UserRepository repository;

    @RequestMapping("")
    public List<User> getAll() {
        return repository.findAll();
    }

    @RequestMapping("/{id}")
    public User getUserById(@PathVariable Integer id) {
        return repository.findOne(id);
    }

    @Autowired
    public void setRepository(UserRepository repository) {
        this.repository = repository;
    }
}
